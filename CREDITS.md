# Credits

## Current maintainers

### Julien Stébenne

The current owner of the module.

- GitLab: [GitLab.com/jstebenne](https://gitlab.com/jstebenne)
- Discord: The Bird#8334
- Ko-fi: [Ko-fi.com/thebird956](https://ko-fi.com/thebird956)

### Professor Geeksworth

- GitLab: [GitLab.com/geekswordman](https://gitlab.com/geekswordsman)
- Discord: Geekswordsman#3085

## Used modules

### About time

We heavily rely on [About-Time](https://gitlab.com/tposney/about-time) by [Tim Posney](https://gitlab.com/tposney) to keep track of time.

### FXMaster

To add weather effects we use [FXMaster](https://gitlab.com/mesfoliesludiques/foundryvtt-fxmaster) by U~[man](https://gitlab.com/mesfoliesludiques).

## Original Author

This module would not exist without the work of [DasSauerkraut](https://github.com/DasSauerkraut). Everything thing done in version 3.1.1 and ealier was made by him and other [contributors](https://github.com/DasSauerkraut/calendar-weather/graphs/contributors).

## And many others

You can get a full list of contributors by going to the project's [Contributors](https://gitlab.com/jstebenne/foundryvtt-weather-control/-/graphs/master) page.
